# Android app with SQLite DB and ContentProvider
## How to use app
### Insert:
insert model and price and click INSERT button.
After Insert is done, the list will be updated automatically.
### Search:
Start writing in EditText
Each change will update the list.
### Test ContentProvider:
Click the button "SELECT ALL WITH CONTENTPROVIDER"
The data will be accessed with ContentProvider and the List will be updated
## How to access Database from other applications
Example Code:
```java
Cursor cursor = getContentResolver().query(
        Uri.parse("content://com.example.appwithcontentprovider.ExampleProvider/SMARTPHONES/0"),
        null,
        null,
        null,
        null);
```
The cursor will return the whole list.