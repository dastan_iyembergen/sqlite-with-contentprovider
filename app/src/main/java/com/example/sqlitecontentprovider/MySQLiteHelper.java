package com.example.sqlitecontentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ListView;

public class MySQLiteHelper extends SQLiteOpenHelper {
    static public String EX_DB = "SMARTPHONES";
    static public String TABLE_NAME = "SMARTPHONES";
    static public int DBVER = 1;

    public MySQLiteHelper(Context context){
        super(context, EX_DB, null, DBVER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "MODEL TEXT, "
                + "PRICE INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
