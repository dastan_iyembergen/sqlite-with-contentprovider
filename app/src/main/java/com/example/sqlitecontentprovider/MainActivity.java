package com.example.sqlitecontentprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    SQLiteOpenHelper mySqlHelper;
    SQLiteDatabase db;
    public static MainActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ExampleProvider myProvider = new ExampleProvider();

        activity = this;

        listView = findViewById(R.id.list);

        mySqlHelper = new MySQLiteHelper(this);
        db = mySqlHelper.getWritableDatabase();

        update_list(null);

        Button insert_bt = findViewById(R.id.insert);
        insert_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText model_ed = findViewById(R.id.insert_model);
                EditText price_ed = findViewById(R.id.insert_price);

                ContentValues new_phone = new ContentValues();
                new_phone.put("MODEL", model_ed.getText().toString());
                new_phone.put("PRICE" , price_ed.getText().toString());
                db.insert(MySQLiteHelper.EX_DB, null, new_phone);
                update_list(null);
            }
        });

        EditText select_model = findViewById(R.id.select_model);
        select_model.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                update_list(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriExample = (new Uri.Builder())
                        .authority("com.example.appwithcontentprovider.E")
                        .scheme("SMARTPHONES")
                        .build();

                // Queries the user dictionary and returns results
                Cursor cursor = getContentResolver().query(
                        Uri.parse("content://com.example.appwithcontentprovider.ExampleProvider/SMARTPHONES/0"), // The content URI
                        null, // The columns to return for each row
                        null, // Selection criteria
                        null, // Selection criteria
                        null); // The sort order for the returned rows

                ArrayList<String> listitems = new ArrayList<>();
                while (cursor.moveToNext()){
                    listitems.add(cursor.getString(0) + ". "
                            + cursor.getString(1) + "; "
                            + "price: " + cursor.getString(2));
                }

                ListAdapter ad = new ArrayAdapter<String>(activity,
                        android.R.layout.simple_list_item_1,
                        listitems);

                listView.setAdapter(ad);
            }
        });
    }

    public void update_list(String model_like){
        String where_condition = "";
        if (model_like != null){
            where_condition = " where MODEL LIKE '" + model_like + "%'";
        }
        Cursor cursor = db.rawQuery("SELECT * FROM " + MySQLiteHelper.TABLE_NAME
                + where_condition, null);
        ArrayList<String> listitems = new ArrayList<>();

        while (cursor.moveToNext()){
            listitems.add(cursor.getString(0) + ". "
                    + cursor.getString(1) + "; "
                    + "price: " + cursor.getString(2));
        }

        ListAdapter ad = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listitems);

        listView.setAdapter(ad);
    }
}
